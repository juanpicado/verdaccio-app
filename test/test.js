#!/usr/bin/env node

/* jshint esversion: 6 */
/* global it:false */
/* global xit:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

require('chromedriver');

var execSync = require('child_process').execSync,
    ejs = require('ejs'),
    expect = require('expect.js'),
    fs = require('fs'),
    mkdirp = require('mkdirp'),
    path = require('path'),
    RegClient = require('npm-registry-client'),
    rimraf = require('rimraf'),
    spawnSync = require('child_process').spawnSync,
    superagent = require('superagent'),
    webdriver = require('selenium-webdriver');

var by = require('selenium-webdriver').By,
    until = require('selenium-webdriver').until,
    Key = require('selenium-webdriver').Key,
    Builder = require('selenium-webdriver').Builder;

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

describe('Application life cycle test', function () {
    this.timeout(0);
    var server, browser = new Builder().forBrowser('chrome').build();
    var LOCATION = 'test';
    var app;
    var username = process.env.USERNAME;
    var password = process.env.PASSWORD;
    var TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 5000;
    var email, token;

    before(function (done) {
        if (!process.env.USERNAME) return done(new Error('USERNAME env var not set'));
        if (!process.env.PASSWORD) return done(new Error('PASSWORD env var not set'));

        var seleniumJar= require('selenium-server-standalone-jar');
        var SeleniumServer = require('selenium-webdriver/remote').SeleniumServer;
        server = new SeleniumServer(seleniumJar.path, { port: 4444 });
        server.start();

        done();
    });

    after(function (done) {
        browser.quit();
        server.stop();
        done();
    });

    function login(username, password, done) {
        browser.get('https://' + app.fqdn).then(function () {
            browser.manage().deleteAllCookies();
            browser.executeScript('localStorage.clear();');
            browser.executeScript('sessionStorage.clear();');
            return browser.get('https://' + app.fqdn);
        }).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//button/span[contains(text(), "Login")]', TIMEOUT)));
        }).then(function () {
            return browser.findElement(by.xpath('//button/span[contains(text(), "Login")]')).click();
        }).then(function () {
            return browser.findElement(by.xpath('//input[@placeholder="Type your username"]')).sendKeys(username);
        }).then(function () {
            return browser.findElement(by.xpath('//input[@placeholder="Type your password"]')).sendKeys(password);
        }).then(function () {
            return browser.findElement(by.xpath('//button[contains(@class, "login-button")]/span[text()="Login"]')).click();
        }).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//button/span[contains(text(), "Logout")]', TIMEOUT)));
        }).then(function () {
            done();
        });
    }

    function checkPackageVisible(done) {
        browser.get('https://' + app.fqdn).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//h1[contains(text(), "verdaccio-test")]', TIMEOUT)));
        }).then(function () {
            done();
        });
    }

    function checkNoPackages(done) {
        browser.get('https://' + app.fqdn).then(function () {
            browser.manage().deleteAllCookies();
            browser.executeScript('localStorage.clear();');
            browser.executeScript('sessionStorage.clear();');
            return browser.get('https://' + app.fqdn);
        }).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//h1[contains(text(), "No Package Published Yet")]', TIMEOUT)));
        }).then(function () {
            done();
        });
    }

    function npmLogin(u, p, e, callback) {
        var config = {
            ssl: {
                strict: false
            }
        };

        var client = new RegClient(config);

        var params = {
            timeout: 5000,
            auth: {
                username: u,
                password: p,
                email: e,
                alwaysAuth: true
            },
        };

        client.get(`https://${app.fqdn}/npm`, params, function (error, data, raw, res) {
            callback(error);
        });
    }

    function publishPackage(u, p, e, done) {
        var config = {
            ssl: {
                strict: false
            }
        };

        var client = new RegClient(config);

        var params = {
            timeout: 5000,
            auth: {
                username: u,
                password: p,
                email: e,
                alwaysAuth: true
            },
            metadata: require('./package.json'),
            body: fs.createReadStream('./package.json') // not really a tarball, but doesn't matter
        };

        client.publish(`https://${app.fqdn}/npm`, params, function (error, data, raw, res) {
            console.log(error);
            console.log(data);
            console.log(raw);
            done(error);
        });
    }

    xit('build app', function () {
        execSync('cloudron build', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can login', function (done) {
        var inspect = JSON.parse(execSync('cloudron inspect'));

        superagent.post('https://' + inspect.apiEndpoint + '/api/v1/developer/login').send({
            username: username,
            password: password
        }).end(function (error, result) {
            if (error) return done(error);
            if (result.statusCode !== 200) return done(new Error('Login failed with status ' + result.statusCode));

            token = result.body.accessToken;

            superagent.get('https://' + inspect.apiEndpoint + '/api/v1/profile')
                .query({ access_token: token }).end(function (error, result) {
                if (error) return done(error);
                if (result.statusCode !== 200) return done(new Error('Get profile failed with status ' + result.statusCode));

                email = result.body.email;
                done();
            });
        });
    });

    it('install app', function () {
        execSync('cloudron install --new --wait --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get app information', function () {
        var inspect = JSON.parse(execSync('cloudron inspect'));

        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];

        expect(app).to.be.an('object');
    });

    it('does not allow external users', function (done) {
        npmLogin('test', 'test', 'test@test.com', function (error) {
            expect(error.message).to.contain('authorization required to access package npm');
            done();
        });
    });

    it('can login using npm', function (done) {
        npmLogin(username, password, email, done);
    });

    it('can publish package', function (done) {
        publishPackage(username, password, email, done);
    });

    it('cannot view package on website when not logged in', checkNoPackages);
    it('can login', login.bind(null, username, password));
    it('can view package on website', checkPackageVisible);

    it('can restart app', function (done) {
        execSync('cloudron restart --wait');
        done();
    });

    it('cannot view package on website when not logged in', checkNoPackages);
    it('can login', login.bind(null, username, password));
    it('can view package on website', checkPackageVisible);

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('restore app', function () {
        execSync('cloudron restore --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('cannot view package on website when not logged in', checkNoPackages);
    it('can login', login.bind(null, username, password));
    it('can view package on website', checkPackageVisible);

    it('move to different location', function () {
        browser.manage().deleteAllCookies();
        execSync('cloudron configure --wait --location ' + LOCATION + '2', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    });

    it('cannot view package on website when not logged in', checkNoPackages);
    it('can login', login.bind(null, username, password));
    it('can view package on website', checkPackageVisible);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    // test update
    it('can install app', function () {
        execSync('cloudron install --new --wait --appstore-id org.eggertsson.verdaccio --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        expect(app).to.be.an('object');
    });
    it('can login using npm', function (done) {
        npmLogin(username, password, email, done);
    });
    it('can publish package', function (done) {
        publishPackage(username, password, email, done);
    });

    it('can update', function () {
        execSync('cloudron install --wait --app ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can login', login.bind(null, username, password));
    it('can view package on website', checkPackageVisible);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });
});
