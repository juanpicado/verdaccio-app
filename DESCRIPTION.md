This app packages Verdaccio <upstream>3.11.4</upstream>

### About

Verdaccio is a private/caching npm repository server

Verdaccio allows you to have a local npm registry with zero configuration. You don't have to install and replicate an entire CouchDB database. Verdaccio keeps its own small database and, if a package doesn't exist there, it asks npmjs.org for it keeping only those packages you use.

- [Verdaccio Git](https://github.com/verdaccio/verdaccio)
- [Package maintainer](https://www.eggertsson.org)

