FROM cloudron/base:1.0.0@sha256:147a648a068a2e746644746bbfb42eb7a50d682437cead3c67c933c546357617

RUN mkdir -p /app/code
WORKDIR /app/code

RUN curl -L https://github.com/verdaccio/verdaccio/archive/v3.11.4.tar.gz | tar -vxz --strip-components 1 -f -
# verdaccio really requires yarn!
RUN npm install -g yarn && \
    yarn install --production=false && \
    yarn run code:build && \
    yarn run build:webui && \
    yarn cache clean && \
    yarn install --production=true --pure-lockfile && \
    yarn add --frozen-lockfile sinopia-ldap

COPY yaml-override.js /app/code/yaml-override.js

COPY start.sh config.yaml.template /app/code/

CMD [ "/app/code/start.sh" ]
