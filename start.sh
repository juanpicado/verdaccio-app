#!/bin/bash

set -eu

mkdir -p /run/verdaccio

echo "Generating config file"
sed -e "s,##LDAP_URL,${LDAP_URL}," \
    -e "s/##LDAP_PORT/${LDAP_PORT}/" \
    -e "s/##LDAP_BIND_DN/${LDAP_BIND_DN}/" \
    -e "s/##LDAP_BIND_PASSWORD/${LDAP_BIND_PASSWORD}/" \
    -e "s/##LDAP_USERS_BASE_DN/${LDAP_USERS_BASE_DN}/" \
    -e "s,##APP_ORIGIN,${APP_ORIGIN}," \
    /app/code/config.yaml.template > /run/verdaccio/config.yaml

if [[ ! -f /app/data/config.yaml ]]; then
    echo "# Add additional customizations in this file" > /app/data/config.yaml
fi

echo "Changing ownership"
chown -R cloudron:cloudron /app/data /run/verdaccio

# merge user yaml file (yaml does not allow key re-declaration)
/usr/local/bin/gosu cloudron:cloudron node /app/code/yaml-override.js /run/verdaccio/config.yaml /app/data/config.yaml

echo "Starting Verdaccio"

exec /usr/local/bin/gosu cloudron:cloudron /app/code/bin/verdaccio --config /run/verdaccio/config.yaml --listen 0.0.0.0:6823
